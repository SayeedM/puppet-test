package com.impostercorp.puppet.controllers;

import com.impostercorp.puppet.dtos.Message;
import com.impostercorp.puppet.dtos.OutputMessage;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Date;

/**
 * Created by sayeedm on 3/1/17.
 */
@Controller
@RequestMapping("/")
public class ChatController {

    @RequestMapping(method = RequestMethod.GET)
    public String viewApplication() {
        return "index";
    }

    @MessageMapping("/chat")
    @SendTo("/topic/message")
    public OutputMessage sendMessage(Message message) {
        System.out.println("here");
        return new OutputMessage(message, new Date());
    }
}
