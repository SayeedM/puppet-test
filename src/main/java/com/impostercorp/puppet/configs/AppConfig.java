package com.impostercorp.puppet.configs;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.web.servlet.mvc.Controller;

/**
 * Created by sayeedm on 3/1/17.
 */
@Configuration
@ComponentScan(basePackages = "com.impostercorp.puppet")
//, excludeFilters = {
//        @ComponentScan.Filter(value = Controller.class, type = FilterType.ANNOTATION),
//        @ComponentScan.Filter(value = Configuration.class, type = FilterType.ANNOTATION)
//})
public class AppConfig {
}
